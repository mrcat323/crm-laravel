<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request\Create as CreateRequest;
use App\Models\OperatorRequest;
use App\Models\Request as RequestModel;
use App\Models\StatusChangesHistory;
use Illuminate\Http\Request;

class RequestController extends Controller
{
    public function index()
    {
        $user = auth('api')->user();
        $requests = RequestModel::with(['operator:id,name'])
                            ->when($user->type == 'operator', fn ($query) =>
                                $query->where('operator_id', $user->id)
                            )->latest()
                            ->get();

        $updatedRequests = $requests->map(function ($request) use ($user) {
            $request->operator_comment = OperatorRequest::where('operator_id', $user->id)
                                                        ->where('request_id', $request->id)
                                                        ->latest()
                                                        ->first();

            return $request;
        });

        return response()->json($updatedRequests);
    }
    
    public function store(CreateRequest $request)
    {
        $operator = auth('api')->user();

        $requestModel = RequestModel::create([
            ...$request->only(['account', 'full_name', 'phone']),
            'status' => 'new',
            'operator_id' => $request->operator_id ?? $operator->id,
        ]);

        if ($request->comment) {
            OperatorRequest::create([
                'operator_id' => $operator->id,
                'request_id' => $requestModel->id,
                'comment' => $request->comment
            ]);
        }

        return response()->json([
            'success' => true
        ]);
    }

    public function show($id)
    {
        $request = RequestModel::where('id', $id)
                            ->when(auth('api')->user()->type == 'operator', fn ($query) => 
                                $query->where('operator_id', auth('api')->user()->id)
                            )
                            ->first();

        $request->operator_comment = OperatorRequest::where('operator_id', auth('api')->user()->id)
                                                    ->where('request_id', $id)
                                                    ->latest()
                                                    ->first();

        if (!$request) {
            return response()->json([
                'message' => 'Not Found'
            ], 404);
        }

        return response()->json($request);
    }

    public function update(Request $request)
    {
        $user = auth('api')->user();
        $requestModel = RequestModel::where('id', $request->id)
                        ->when($user->type == 'operator', fn ($query) => $query->where('operator_id', $user->id))
                        ->first();

        if (!$requestModel) {
            return response()->json([
                'message' => 'Not Found'
            ], 404);
        }

        StatusChangesHistory::create([
            'operator_id' => $user->id,
            'request_id' => $requestModel->id,
            'pre_status' => $requestModel->status,
            'status' => $request->status
        ]);

        $requestModel->update([
            'operator_id' => $request->operator_id ?? $requestModel->operator_id,
            'status' => $request->status,
        ]);


        OperatorRequest::create([
            'operator_id' => $user->id,
            'request_id' => $requestModel->id,
            'comment' => $request->input('operator_comment.comment'),
            'status' => $request->status
        ]);

        return response()->json($requestModel);
    }

    public function search(Request $request)
    {
        $user = auth('api')->user();
        $requests = RequestModel::with(['operator', 'operatorRequests' => fn ($query) => 
                                $query->where('operator_id', $user->id)
                            ])
                            ->when(in_array($request->type, ['account', 'full_name', 'phone']),
                                fn ($query) => 
                                    $query->where($request->type, $request->value)
                                )->when($user->type == 'operator', fn ($query) =>
                                    $query->where('operator_id', $user->id)
                                )->latest('id')
                                ->get();

        return response()->json($requests);
    }
}
