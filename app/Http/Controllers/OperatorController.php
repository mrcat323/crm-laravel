<?php

namespace App\Http\Controllers;

use App\Models\Operator;
use Illuminate\Http\Request;

class OperatorController extends Controller
{
    public function index()
    {
        $operators = Operator::where('type', 'operator')
                            ->active()
                            ->latest()
                            ->get();

        return response()->json($operators);
    }
}
