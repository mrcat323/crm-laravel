<?php

namespace App\Http\Controllers;

use App\Models\OperatorRequest;
use App\Models\StatusChangesHistory;
use Illuminate\Http\Request;

class StatusChangeHistoryController extends Controller
{
    public function index()
    {
        $history = StatusChangesHistory::with(['operator', 'request'])->latest()->get();

        $updatedHistory = $history->map(function ($element) {
            $element->operator_comment = OperatorRequest::where('request_id', $element->request_id)
                                                        ->where('status', $element->status)
                                                        ->latest()
                                                        ->first();

            return $element;
        });

        return response()->json($updatedHistory);
    }
}
