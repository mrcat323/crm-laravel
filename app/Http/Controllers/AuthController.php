<?php

namespace App\Http\Controllers;

use App\Http\Requests\Operator\Login as LoginRequest;
use App\Models\Operator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $operator = Operator::where('login', $request->login)
                            ->active()
                            ->first();

        if (!$operator) {
            return response()->json([
                'messages' => [
                    'login' => __('messages.user_does_not_exist')
                ]
            ], 404);
        }

        if (!Hash::check($request->password, $operator->password)) {
            return response()->json([
                'messages' => [
                    'password' => __('messages.password_mismatch')
                ]
            ], 401);
        }

        $token = $operator->createToken(env('TOKEN_KEY'))->plainTextToken;

        return response()->json($token);
    }

    public function logout()
    {
        auth('api')->user()->tokens()->delete();

        return response()->json([
            'message' => __('messages.logged_out')
        ]);
    }
}
