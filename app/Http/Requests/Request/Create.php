<?php

namespace App\Http\Requests\Request;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class Create extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'account' => 'required|numeric',
            'full_name' => 'required',
            'phone' => 'required',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $response = response()->json(['messages' => $validator->messages()], 400);

        throw new ValidationException($validator, $response);
    }
}
