<?php

namespace App\Helpers;

trait Handy
{
    public function scopeActive($query)
    {
        return $query->where('active', true);
    }
}