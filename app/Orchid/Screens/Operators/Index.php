<?php

namespace App\Orchid\Screens\Operators;

use App\Models\Operator;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class Index extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(): iterable
    {
        return [
            'operators' => Operator::latest()->get()
        ];
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return 'Index';
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            ModalToggle::make(__('interface.create'))
                    ->icon('plus')
                    ->modal('createOperator')
                    ->method('create')
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        return [
            Layout::table('operators', [
                TD::make('id', __('ID')),
                TD::make('name', __('interface.name')),
                TD::make('login', __('interface.login')),
                TD::make('active', __('interface.activeness'))
                    ->render(fn (Operator $operator) => 
                        $operator->active ? __('interface.active') : __('interface.deactive')
                ),
                TD::make('created_at', __('interface.created_date'))
                    ->render(fn (Operator $operator) => 
                        Carbon::parse($operator->created_at)->format('Y-m-d')
                    ),
                TD::make('actions', __('interface.actions'))
                    ->render(fn (Operator $operator) => 
                        DropDown::make()
                                ->icon('three-dots-vertical') 
                                ->list([
                                    $operator->active ?
                                        Button::make(__('interface.deactivate'))
                                            ->method('deactivate')
                                            ->parameters([
                                                'id' => $operator->id
                                            ]) :
                                        Button::make(__('interface.activate'))
                                            ->method('activate')
                                            ->parameters([
                                                'id' => $operator->id
                                            ])
                                ])
                    )
            ]),
            Layout::modal('createOperator', Layout::rows([
                Input::make('name')
                    ->title(__('interface.name')),
                Input::make('login')
                    ->title(__('interface.login')),
                Input::make('password')
                    ->title(__('interface.password'))
                    ->type('password'),
                Select::make('type')
                    ->title(__('interface.type'))
                    ->options([
                        'operator' => __('interface.simple_operator'),
                        'backoffice' => __('interface.backoffice_operator'),
                    ])
            ]))
        ];
    }

    public function activate(Request $request)
    {
        Operator::where('id', $request->id)
                ->update([
                    'active' => true
                ]);

        Toast::info(__('messages.activated'));
    }


    public function deactivate(Request $request)
    {
        Operator::where('id', $request->id)
                ->update([
                    'active' => false
                ]);

        Toast::info(__('messages.deactivated'));
    }

    public function create(Request $request)
    {
        Operator::create([
            ...$request->only(['name', 'login', 'type']),
            'password' => Hash::make($request->input('password'))
        ]);

        Toast::info(__('messages.created'));
    }
}
