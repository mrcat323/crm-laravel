<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperatorRequest extends Model
{
    use HasFactory;

    protected $fillable = [
        'operator_id',
        'request_id',
        'comment',
        'status',
    ];
}
