<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    use HasFactory;

    protected $fillable = [
        'operator_id',
        'account',
        'full_name',
        'phone',
        'status',
        'comment',
    ];

    public function operator()
    {
        return $this->belongsTo(Operator::class);
    }

    public function operatorRequests()
    {
        return $this->belongsToMany(OperatorRequest::class, 'operator_requests');
    }

    protected function createdAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value, $attributes) => Carbon::parse($value)->format('Y-m-d H:i')
        );
    }
}
