<?php

namespace App\Models;

use App\Helpers\Handy as HandyHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;
use Orchid\Screen\AsSource;

class Operator extends Model
{
    use HasFactory, HasApiTokens, AsSource, HandyHelper;

    protected $fillable = [
        'name',
        'login',
        'password',
        'type',
        'active',
    ];

    protected $hidden = [
        'password'
    ];

    public function operatorRequests()
    {
        return $this->belongsToMany(OperatorRequest::class);
    }

    public function requests()
    {
        return $this->hasMany(Request::class);
    }
}
