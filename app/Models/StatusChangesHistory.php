<?php

namespace App\Models;

use App\Models\Request as RequestModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusChangesHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'operator_id',
        'request_id',
        'pre_status',
        'status'
    ];

    public function request()
    {
        return $this->belongsTo(RequestModel::class);
    }

    protected function createdAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value, $attributes) => Carbon::parse($value)->format('Y-m-d H:i')
        );
    }

    public function operator()
    {
        return $this->belongsTo(Operator::class);
    }
}
