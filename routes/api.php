<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\OperatorController;
use App\Http\Controllers\RequestController;
use App\Http\Controllers\StatusChangeHistoryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return auth('api')->user();
});

Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/requests', [RequestController::class, 'index'])->name('requests.all');
    Route::get('/request/{id}', [RequestController::class, 'show'])->name('requests.show');
    Route::post('/requests/store', [RequestController::class, 'store'])->name('requests.store');
    Route::patch('/requests/update', [RequestController::class, 'update'])->name('requests.update');
    Route::get('/requests/search', [RequestController::class, 'search'])->name('requests.search');
    Route::get('/operators', [OperatorController::class, 'index'])->name('operators.all');
    Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('/history', [StatusChangeHistoryController::class, 'index']);
});