#!/bin/bash

set -e

chown -Rv www-data:www-data /var/www/html/storage/
composer install && \
npm install --force && \
npm run build && \
php artisan cache:clear && \
php artisan migrate --force && \
php-fpm