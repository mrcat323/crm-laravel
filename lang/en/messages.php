<?php

return [
    'user_does_not_exist' => 'User does not exist',
    'password_mismatch' => 'Password is incorrect',
    'logged_out' => 'Logged out successfully',
];