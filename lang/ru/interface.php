<?php

return [
    'name' => 'Имя',
    'simple_operator' => 'Обычный оператор',
    'backoffice_operator' => 'Оператор бэк-офиса',
    'create' => 'Добавить',
    'activate' => 'Активировать',
    'deactivate' => 'Деактивировать',
    'login' => 'Логин',
    'type' => 'Тип',
    'password' => 'Пароль',
    'created_date' => 'Дата создания',
    'actions' => 'Действие',
    'activeness' => 'Активность',
    'active' => 'Активный',
    'deactive' => 'Неактивный',
];