<?php

return [
    'user_does_not_exist' => 'Пользователь не существует',
    'password_mismatch' => 'Пароль неправильный для входа',
    'logged_out' => 'Выход был осуществлен',
    'activated' => 'Активировано',
    'deactivated' => 'Дизактивировано',
    'created' => 'Создано',
];