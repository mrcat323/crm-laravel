## Начало
Чтобы развернуть проект Вам необходимо установить Docker и Docker-Compose.
Копируем `.env.example`:
```shell
cp .env.example .env
```
Дальше надо копировать `.env.example` в папке `docker` чтобы Docker мог функционировать:
```shell
cd docker && cp .env.example .env
```
Далее запускаем Docker-Compose:
```shell
docker-compose up --build -d
```
Вроде проект завернут, теперь нам надо создать админа:
```shell
docker-compose exec app php artisan orchid:admin Admin
```
Вам будет показаны поля которые надо заполнить, после заполнение и создание админа зайдите на http://localhost:8081/admin

Есть возникли вопросы, пишите в [Telegram](https://t.me/mrcat323)